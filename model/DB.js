const mysql = require('mysql')

class DB {
    constructor () {
        this.con = mysql.createConnection({
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            database: process.env.DB_NAME
        });
    }

    query (sql, cb) {
        try {
            this.con.query(sql, cb);
        } catch (error) {
            console.log(error)            
        }
    }

}

module.exports = new DB