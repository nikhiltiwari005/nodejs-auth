const express = require('express')
const layouts = require('express-ejs-layouts')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const flash = require('express-flash')
const app = express()

require('dotenv').config()

app.use(express.static(__dirname + '/public'))
app.use(layouts)
app.set('view engine', 'ejs')
app.use(express.urlencoded({extended: true}))
app.use(cookieParser())
app.use(session({
    secret: 'Your_Secret_Key', 
    resave: true, 
    saveUninitialized: true
}))
app.use(flash())

app.use((req, res, next) => {
    res.locals.user = req?.session?.user ?? {}
    if(res.locals.user?.name && (req.path == '/login' || req.path == '/register'))
        return res.redirect('/');
    next()
})

require('./routes')(app)

app.listen(3000);