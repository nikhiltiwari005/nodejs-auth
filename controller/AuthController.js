// 2
const Bcrypt = require('bcrypt');
const DB = require('../model/DB')

class Auth {

    login (req, res) {
        res.render('auth/login')
    }

    register (req, res) {
        res.render('auth/register')
    }

    logout (req, res) {
        if (req?.session?.user) {
            req.session.destroy();
        }
        return res.redirect('/');
    }

    async get (req, res) {
        let sql = `SELECT * FROM users WHERE email = '${req.body.email}'`;
        DB.query(sql, async (err, result) => {
            if (err) {
                console.log(err)
                return false;
            }
            if (req.body.email !== result[0]?.email) {
                req.flash('danger', "Not Registered!!")
                return res.redirect('/login')
            }
                
            if (! await Bcrypt.compare(req.body.password, result[0]?.password)) {
                req.flash('danger', "Incorrect Password!!")
                return res.redirect('/login')
            }

            req.session.user = result[0];
            req.flash('success', 'You are LoggedIn!!')
            return res.redirect('/')
        });
    }

    async store (req, res) {
        try {
            const hashPassword = await Bcrypt.hash(req.body.password, 10)
            let sql = `INSERT INTO users (name, email, password) VALUES('${req.body.name}', '${req.body.email}', '${hashPassword}')`;
            DB.query(sql, (err, result) => {
                if (err) {
                    req.flash('warning', err.message)
                    return res.redirect('/register');
                }
                if (result.affectedRows == 1) {
                    req.flash('success', 'Registered Successfully')
                    return res.redirect('/login')
                }
            });
        } catch (error) {
            console.log(error.message)
        }
    }
}

module.exports = new Auth

// // 1
// exports.login = (req, res) => {
//     res.render('auth/login')
// }

// exports.register = (req, res) => {
//     res.render('auth/register')
// }


// // 3
// module.exports = {
//     login : (req, res) => {
//         res.render('auth/login')
//     },
//     register: (req, res) => {
//         res.render('auth/register')
//     }
// }
