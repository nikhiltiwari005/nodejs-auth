let AuthController  = require('./controller/AuthController')
module.exports = (app, options = {}) => {
    app.get('/', (req, res) => {
        return res.render('index.ejs')
    });
    app.get('/login', AuthController.login)
    app.get('/register', AuthController.register)
    app.post('/login', AuthController.get)
    app.post('/register', AuthController.store)
    app.get('/logout', AuthController.logout)
}

